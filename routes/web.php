<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();

Route::get('/', function () {
    return view('auth/login');
});

Route::get('profile', 'UserController@profile');

Route::post('profile', 'UserController@update_avatar');

Route::get('/index','SensoresController@SensoresCuartos');


Route::get('/humedad', function () {
    return view('contenido/humedad');
});
Route::get('/temperature', function () {
    return view('contenido/temperatura');
});

Route::get('/TempHum', 'TemperaturaController@TempHum' );
Route::get('/time', 'TemperaturaController@tiempo' );
Route::get('/Hum', 'TemperaturaController@humedad' );
Route::get('/sensores', 'SensoresController@SensoresCuartos' );
Route::get('/cuartos', 'SensoresController@Cuartos' );

Route::get('/home', 'HomeController@index')->name('home');
