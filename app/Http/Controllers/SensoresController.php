<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Sensores;
use App\Cuartos;
use Response;

class SensoresController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function SensoresCuartos()
    {
        //$temp = Temperatura::all();
        $rooms =cuartos::select('nombre')->get();
        $sens  = Sensores::select('sensores.id','sensores.tipo','sensores.estado','cuartos.nombre')->join('cuartos','cuartos.IDsensor','=','sensores.id')->orderBy('id','asc')->get();

        return view('contenido/index')->with(compact(['sens','rooms']));
    }

    public function Cuartos()
    {
        //$temp = Temperatura::all();
        $rooms = Sensores::select('sensores.id','sensores.tipo','sensores.estado','sensores.accesos','cuartos.nombre')->join('cuartos','cuartos.IDsensor','=','sensores.id')->orderBy('id','asc')->get();

        return Response::json($rooms);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
