<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cuartos;

class CuartosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function Cuartos()
    {
        //$temp = Temperatura::all();
        $rooms = Cuartos::select('nombre')->get();

        return view('contenido/index')->with(compact('rooms'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
