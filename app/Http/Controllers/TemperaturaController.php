<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Temperatura;


class TemperaturaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function TempHum()
    {
        //$temp = Temperatura::all();
        $temp = Temperatura::orderBy('created_at','desc')->take(1)->get();
        return $temp;
    }

    public function temperatura()
    {
        //$temp = Temperatura::all();
        $hum = Temperatura::select('temp','created_at')->take(100)->get();
        return $temp;
    }

    public function humedad()
    {

            //$hum = Temperatura::select('hum','temp')->orderBy('id','desc')->take(1)->get();
            $hum = Temperatura::select('hum','temp')->orderBy('id','desc')->get();
            return $hum;
            //$id = Temperatura::select('id')->take(1)->get();
        


    }

    public function tiempo()
    {
        $temp = Temperatura::all();
        return $temp;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
