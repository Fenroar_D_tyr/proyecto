<nav class="navbar default-layout-navbar col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
  <div class="text-center navbar-brand-wrapper d-flex align-items-center justify-content-center">
    <a class="navbar-brand brand-logo" href="/index"><img src="images/logoSHS.png" alt="logo" style="width: 45%; height: 45%; margin-top: 45px;"></a>
    <a class="navbar-brand brand-logo-mini" href="/index"><img src="images/logo-miniSHS.png" alt="logo" style="width: 35px; height: 35px;"/></a>
  </div>
  <div class="navbar-menu-wrapper d-flex align-items-stretch">
    <ul class="navbar-nav navbar-nav-right">
      <li class="nav-item nav-profile dropdown">
        <a class="nav-link dropdown-toggle" id="profileDropdown" href="#" data-toggle="dropdown" aria-expanded="false">
          <div class="nav-profile-img">
            <img src="/uploads/avatars/{{ Auth::user()->avatar }}" style=" width=:32px; height:32px; position: absolute; border-radius:50%;">
            <span class="availability-status online"></span>
          </div>
          <div class="nav-profile-text">
            <p class="mb-1 text-black">{{ auth()->user()->name }}</p>
          </div>
        </a>
        <div class="dropdown-menu navbar-dropdown" aria-labelledby="profileDropdown">

            <a class="dropdown-item" href="{{ '/profile' }}">
              <i class="mdi mdi-account mr-2 text-primary"></i>
              {{ __('Perfil') }}
            </a>

            <a class="dropdown-item" href="{{ route('logout') }}"
              onclick="event.preventDefault();
              document.getElementById('logout-form').submit();">
              <i class="mdi mdi-logout mr-2 text-primary"></i>
              {{ __('Logout') }}
            </a>

            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            @csrf
            </form>
        </div>
      </li>
      <li class="nav-item d-none d-lg-block full-screen-link">
        <a class="nav-link">
          <i class="mdi mdi-fullscreen" id="fullscreen-button"></i>
        </a>
      </li>
      <li class="nav-item nav-settings d-none d-lg-block">
        <a class="nav-link" href="#">
          <i class="mdi mdi-format-line-spacing"></i>
        </a>
      </li>
    </ul>
    <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas">
      <span class="mdi mdi-menu"></span>
    </button>
  </div>
</nav>
<nav class="sidebar sidebar-offcanvas" id="sidebar">
  <ul class="nav">
    <li class="nav-item nav-profile">
      <a href="#" class="nav-link">
        <div class="nav-profile-image">
          <img src="/uploads/avatars/{{ Auth::user()->avatar }}">
          <span class="login-status online"></span>
        </div>
        <div class="nav-profile-text d-flex flex-column">
          <span class="font-weight-bold mb-2">{{ auth()->user()->name }}</span>
          <span class="text-secondary text-small">Usuario</span>
        </div>
        <i class="mdi mdi-bookmark-check text-success nav-profile-badge"></i>
      </a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="index">
        <span class="menu-title" style="color: #A09F9F;">Dashboard</span>
        <i class="mdi mdi-home menu-icon"></i>
      </a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="temperature">
        <span class="menu-title" style="color: #A09F9F;">Temperatura</span>
        <i class="mdi mdi-temperature-celsius menu-icon" style="color: #ff9093;"></i>
      </a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="humedad">
        <span class="menu-title" style="color: #A09F9F;">Humedad</span>
        <i class="mdi mdi-water-percent menu-icon" style="color: #79acfb;"></i>
      </a>
    </li>
  </ul>
</nav>
