@extends('welcome')
@section('index')
<?php
$connect = mysqli_connect("127.0.0.1", "root", "", "shsproyecto");
$query = "SELECT * FROM temperatura ORDER BY id DESC";
$result = mysqli_query($connect, $query);
$chart_data = '';
while($row = mysqli_fetch_array($result))
{
  $chart_data .= "{ year:'".$row["created_at"]."', profit:".$row["temp"]."}, ";
}
$chart_data = substr($chart_data, 0, -2);
?>
<div class="main-panel">
<div id="container" style="min-width: 100%; height: 400px; margin: 0 auto"></div>
</div>
<script>
Morris.Line({
 element : 'container',
 resize: true,
 data:[<?php echo $chart_data; ?>],
 xkey:'year',
 ykeys:['profit'],
 labels:['Temperatura'],
 hideHover:'auto',
 stacked:true,
 lineColors: ['#ff9093']
});
</script>
@endsection
