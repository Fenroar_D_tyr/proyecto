@extends('welcome')
@section('index')
<?php
$connect = mysqli_connect("127.0.0.1", "root", "", "shsproyecto");
$query = "SELECT * FROM temperatura ORDER BY id DESC";
$result = mysqli_query($connect, $query);
$chart_data = '';
while($row = mysqli_fetch_array($result))
{
  $chart_data .= "{ year:'".$row["created_at"]."', profit:".$row["temp"].", purchase:".$row["hum"]."}, ";
}
$chart_data = substr($chart_data, 0, -2);
?>
<div class="main-panel">
  <div class="content-wrapper" style="background-color:#f8f9fa;">
    <div class="row">
      <div class="col-md-4 stretch-card grid-margin" style="text-align: center; height: 180px;">
        <div class="card card-img-holder text-white" style="box-shadow: 5px 5px 30px 0px rgba(0,0,0,0.09);">
          <div class="card-body">
            <h4 id="intruso" class="mb-3 text-black display-5" style="color: #c4cad8;">Estado</h4>
            <div>
              <img id="escudo" src="images/safe.svg" style="width: 30%; height: 30%;">
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-4 stretch-card grid-margin" style="text-align: center; height: 180px;">
        <div class="card card-img-holder text-white" style="box-shadow: 5px 5px 30px 0px rgba(0,0,0,0.09);">
          <div class="card-body">
            <h4 class="mb-3 text-black display-5" style="color: #c4cad8;">Temperatura</h4>
            <label class="mb-5 display-2" id="temperature"style="color: #ff9093;">°C</label>
          </div>
        </div>
      </div>
      <div class="col-md-4 stretch-card grid-margin" style="text-align: center; height: 180px;">
        <div class="card card-img-holder text-white" style="box-shadow: 5px 5px 30px 0px rgba(0,0,0,0.09);">
          <div class="card-body">
            <h4 class="mb-3 text-black display-5" style="color: #c4cad8;">Humedad</h4>
            <label class="mb-5 text-black display-2" id="humidity"style="color: #79acfb;">%</label>
          </div>
        </div>
      </div>
      <div class="col-md-4 stretch-card grid-margin" style="text-align: center;">
        <div class="card card-img-holder text-white" style="box-shadow: 5px 5px 30px 0px rgba(0,0,0,0.09);">
          <div class="card-body">
         <div class="table-responsive">
            <table id="rooms" class="table table-hover text-black" style="color: #A09F9F;">
              <thead class="text-white" style="background-color: #79acfb; border-color: #79acfb;">
                <tr>
                  <th>Cuarto</th>
                  <th>Seguro</th>
                </tr>
                <tbody>
                  <tr>
                    <td id="nombre1"></td>
                    <td><img id="acceso1" src="" width="30%" heigth=""></td>
                  </tr>
                  <tr>
                    <td id="nombre2"></td>
                    <td><img id="acceso2" src="" width="30%" heigth=""></td>
                  </tr>
                  <tr>
                    <td id="nombre3"></td>
                    <td><img id="acceso3" src="" width="30%" heigth=""></td>
                  </tr>
                  <tr>
                    <td id="nombre4"></td>
                    <td><img id="acceso4" src="" width="30%" heigth=""></td>
                  </tr>
                </tbody>
              </thead>
            </table>
          </div>
          </div>
        </div>
      </div>
      <div class="col-md-8 stretch-card grid-margin" style="text-align: center;">
        <div class="card card-img-holder text-white" style="box-shadow: 5px 5px 30px 0px rgba(0,0,0,0.09);">
          <div class="card-body">
          <div class="table-responsive">
            <table id="tabla" class="table table-hover text-black" style="color: #A09F9F;">
              <thead class="text-white" style="background-color: #79acfb; border-color: #79acfb;">
                <tr>
                  <th>ID</th>
                  <th>Sensor</th>
                  <th>Cuarto</th>
                  <th>Estado</th>
                </tr>
              </thead>
            </table>
          </div>
          </div>
        </div>
      </div>
      <div class="col-md-12 stretch-card grid-margin" style="text-align: center;">
        <div class="card card-img-holder text-white" style="box-shadow: 5px 5px 30px 0px rgba(0,0,0,0.09);">
          <div class="card-body">
            <div id="container"></div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <footer class="footer">
    <div class="d-sm-flex justify-content-center justify-content-sm-between">
    </div>
  </footer>
</div>

 <script>
    $(function () {
          setInterval(function () {
            $.get( "apis/temphum.php?consultar=1", function( UltimosDatos ) {
                    var varlocalx=parseFloat(UltimosDatos[0].x);
                    var varlocaly=parseFloat(UltimosDatos[0].y);

                    document.getElementById("temperature").innerHTML = varlocalx+"°C";
                    document.getElementById("humidity").innerHTML = varlocaly+"%";
           });
           $.get( "apis/cuartos.php?consultar=1", function (data) {
             var varlocalNombre1=(data[0].nombre);
             var varlocalNombre2=(data[1].nombre);
             var varlocalNombre3=(data[2].nombre);
             var varlocalNombre4=(data[3].nombre);
             document.getElementById("nombre1").innerHTML = varlocalNombre1;
             document.getElementById("nombre2").innerHTML = varlocalNombre2;
             document.getElementById("nombre3").innerHTML = varlocalNombre3;
             document.getElementById("nombre4").innerHTML = varlocalNombre4;

             var varlocalAcceso1=(data[0].temp);
             var varlocalAcceso2=parseFloat(data[1].accesos);
             var varlocalAcceso3=parseFloat(data[2].accesos);
             var varlocalAcceso4=parseFloat(data[3].accesos);
             document.getElementById("acceso1").innerHTML = varlocalAcceso1;
             document.getElementById("acceso2").innerHTML = varlocalAcceso3;
             document.getElementById("acceso3").innerHTML = varlocalAcceso3;
             document.getElementById("acceso4").innerHTML = varlocalAcceso4;

             if (varlocalAcceso1 <= 50) {
               document.getElementById("acceso1").src = "images/greenpulse.gif";
             }else{
               document.getElementById("acceso1").src = "images/redpulse.gif";
             }
            if (varlocalAcceso2 == 0) {
              document.getElementById("acceso2").src = "images/greenpulse.gif";
            }else{
              document.getElementById("acceso2").src = "images/redpulse.gif";
            }
            if (varlocalAcceso3 == 1) {
              document.getElementById("acceso3").src = "images/greenpulse.gif";
            }else{
              document.getElementById("acceso3").src = "images/redpulse.gif";
            }
            if (varlocalAcceso4 == 0) {
              document.getElementById("acceso4").src = "images/greenpulse.gif";
            }else{
              document.getElementById("acceso4").src = "images/redpulse.gif";

            }
            if (varlocalAcceso1 > 50 ||varlocalAcceso2 == 1 || varlocalAcceso3 == 0 || varlocalAcceso4 == 1) {
              document.getElementById("escudo").src = "images/alert.svg";
            }else {
              document.getElementById("escudo").src = "images/safe.svg";
            }
           });
          $.get( "apis/cuartos.php?consultar=1", function (sensores) {

                $('#tabla tr').not(':first').remove();
                var html = '';
                for(var i = 0; i < sensores.length; i++){
                html += '<tr style="background:white; color:#A09F9F;">'+
                            '<td>' + sensores[i].id + '</td>' +
                            '<td>' + sensores[i].tipo + '</td>' +
                            '<td>' + sensores[i].nombre + '</td>' +
                            '<td>' + sensores[i].estado + '</td>' +
                        '</tr>';
                     }
                $('#tabla tr').first().after(html);


           });}, 1000);


});
</script>
<script>
Morris.Line({
 element : 'container',
 resize: true,
 data:[<?php echo $chart_data; ?>],
 xkey:'year',
 ykeys:['profit', 'purchase'],
 labels:['Temperatura', 'Humedad'],
 hideHover:'auto',
 stacked:true,
 lineColors: ['#ff9093','#79acfb']
});
</script>

<!--<script>
  window.onload = function() {
    loaddata();
  };
  function loaddata(){
    var url = "/TempHum";

    $.getJSON(url, function(data) {
      var val= data;
      console.log(data);
//      var humid=(data['temperatura'][(Object.keys(data['temperatura']).length)-1]['hum']);
  //    var temper=(data['temperatura'][(Object.keys(data['temperatura']).length)-1]['temp']);
      var temp=data[0].temp;
      var hum=data[0].hum;
      document.getElementById("temperature").innerHTML = temp+"°C";
      document.getElementById("humidity").innerHTML = hum+"%";


  window.setInterval(function(){
    loaddata();
  }, 10000);

    });
  }
</script>-->


@endsection
