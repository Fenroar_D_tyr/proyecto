<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <link href="css/bootstrap.min.css" rel="stylesheet">
        <title>SHS</title>
        <link rel="stylesheet" href="vendors/iconfonts/mdi/css/materialdesignicons.min.css">
        <link rel="stylesheet" href="vendors/css/vendor.bundle.base.css">
        <link rel="stylesheet" href="css/style.css">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        
        <!-- Fonts -->
        <link rel="dns-prefetch" href="https://fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

        <script src="vendors/js/vendor.bundle.base.js"></script>
        <script src="vendors/js/vendor.bundle.addons.js"></script>
        <script src="js/off-canvas.js"></script>
        <script src="js/misc.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/dashboard.js"></script>
        <script src="js/jquery-2.1.4.js"></script>
        <script src="js/highcharts.js"></script>
        <script src="js/exporting.js"></script>
    </head>

    <body>
      <div>
      <div class="container-scroller">
        <div class="container-fluid page-body-wrapper">
          @include('menu.menu')
          <!-- contenido-->

          <div class="container" style="background-color:#f8f9fa;">
            
            <br>
            <br>
            <br>
            
            <div class="row">

              <div class="col-2">
              </div>

              <div class="col-8 text-center">
                <img src="/uploads/avatars/{{ Auth::user()->avatar }}" style=" width=:80px; height:80px; border-radius:50%;">
              </div>

              <div class="col-2">
              </div>

            </div>

            <br>

            <div class="row">

              <div class="col text-center">
                <h3>{{ auth()->user()->name }}</h3>
              </div>

            </div>

            <div class="row">

              <div class="col-12 text-center">
                <form enctype="multipart/form-data" action="/profile" method="POST">
                  <input type="file" name="avatar">
                  <input type="hidden" name="_token" value="{{ csrf_token() }}">
                  <input type="submit" style="margin:10px; background-color: #3490dc; color:white;" class="pull-rigth btn btn-sm">
                </form>
              </div>

            </div>

          </div>

          <!-- fin contenido -->
        </div>
      </div>
      </div>
    </body>
</html>
